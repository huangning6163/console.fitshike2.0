#!/bin/bash
SCRIPT_DIR=$(cd "$(dirname "$0")"; pwd)
source ${SCRIPT_DIR}/env.include.sh
cd ${SCRIPT_DIR}

function usage
{
    echo "usage: ./`basename $0` <serverId|global:0> <open>|<close>"
    exit 1
}

if [ $# -lt 2 ]; then usage; fi

SVR_SET=$1
shift

case "$1" in
open)
    openPre ${SVR_SET}
    ;;
close)
    closePre ${SVR_SET}
    ;;
*)
    usage
    ;;
esac
