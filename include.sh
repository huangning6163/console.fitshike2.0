DEV_toolDir=${DEV_projectDir}/tool
DEV_storageDir=${DEV_projectDir}/app/storage

STAG_projectDir=/var/www/fitshike2
STAG_toolDir=${STAG_projectDir}/tool
STAG_storageDir=${STAG_projectDir}/app/storage

PROD_projectDir=/home/angelhere/projects/fitshike2
PROD_projectPreDir=/home/angelhere/projects/fitshike2_pre
PROD_projectBackendDir=/home/angelhere/projects/fitshike2_backend
PROD_toolDir=${PROD_projectDir}/tool
PROD_toolPreDir=${PROD_projectPreDir}/tool
PROD_storageDir=${PROD_projectDir}/app/storage
PROD_storagePreDir=${PROD_projectPreDir}/app/storage

DATA_stagingDir=${MY_consoleDir}/env.data/staging
DATA_productionDir=${MY_consoleDir}/env.data/production

PROD_IdentityFile="/home/www/angelhere-publish-key"

source ${DEV_projectDir}/tool/env.include.sh
source ${MY_consoleDir}/function/helper.sh