#!/bin/bash

projectHome=/home/angelhere/projects
projectData=/storage/angelhere/projects
projectCode=fitshike2


mkdir ${projectData}
cd ${projectData}/
mkdir ${projectCode} ${projectCode}_pre ${projectCode}_backend


cd ${projectHome}
mkdir ${projectCode} ${projectCode}_pre
cd ${projectData}
mkdir ${projectCode}/upload ${projectCode}/storage ${projectCode}_pre/upload ${projectCode}_pre/storage
cd ${projectData}
chmod 0777 -R ${projectCode}/upload ${projectCode}/storage ${projectCode}_pre/upload ${projectCode}_pre/storage
test ! -h ${projectHome}/${projectCode}_backend && ln -s ${projectData}/${projectCode}_backend ${projectHome}/${projectCode}_backend


cd ${projectHome}/${projectCode}_pre
mkdir app public
test ! -h ${projectHome}/${projectCode}_pre/app/storage && ln -s ${projectData}/${projectCode}_pre/storage  ${projectHome}/${projectCode}_pre/app/storage
cd ${projectHome}/${projectCode}_pre/app/storage
mkdir meta sessions views tmp ocm logs cache
sudo chmod 0777 -R ${projectHome}/${projectCode}_pre/app/storage
test ! -h ${projectHome}/${projectCode}_pre/public/upload && ln -s ${projectData}/${projectCode}_pre/upload  ${projectHome}/${projectCode}_pre/public/upload
chmod 0777 -R ${projectHome}/${projectCode}_pre/public/upload


cd ${projectHome}/${projectCode}
mkdir app public
test ! -h ${projectHome}/${projectCode}/app/storage && ln -s ${projectData}/${projectCode}/storage  ${projectHome}/${projectCode}/app/storage
cd ${projectHome}/${projectCode}/app/storage
mkdir meta sessions views tmp ocm logs cache
sudo chmod 0777 -R ${projectHome}/${projectCode}/app/storage
test ! -h ${projectHome}/${projectCode}/public/upload && ln -s ${projectData}/${projectCode}/upload  ${projectHome}/${projectCode}/public/upload
chmod 0777 -R ${projectHome}/${projectCode}/public/upload