#!/bin/bash
SCRIPT_DIR=$(cd "$(dirname "$0")"; pwd)
source ${SCRIPT_DIR}/env.include.sh
cd ${SCRIPT_DIR}

function notice() {
    echo -ne "\nFBI WARNING:\n1，如果有修改了缓写的meta，发出外网后需要重启该缓写。\n2，确保所有其他人的setup都收集完毕。\n3，。。。\n(Y/N)?"
    read yesno
    if [ "$yesno" != "Y" ] && [ "$yesno" != "y" ];
    then
        echo "ABORTED"
        exit
    fi
}

function usage() {
        echo "usage: ./`basename $0` <serverId|global:0> <web> <source>"
        exit 1
}

function buildWeb() {
    if [ -d ${DATA_productionDir}/project/public ]; then
        #把之前生成过压缩rev版的assets目录清理掉
        rm -rf ${DATA_productionDir}/project/public/assets
    fi

    rsync -av --delete \
        --exclude=".git*" \
        --exclude=".svn*" \
        --exclude="/app/storage" \
        --exclude="/public/source" \
        --exclude="/public/upload" \
        --exclude="/tool/env.*" \
        ${STAG_projectDir}/ ${DATA_productionDir}/project/
    if [ $? -ne 0 ]; then return 1; fi

    #进行对assets文件的压缩rev，并替换views里面的版本号
    prompt_msg '>>Dealing image files.\n' G
    python ${MY_consoleDir}/function/python/checksum.py
    if [ $? -ne 0 ]; then return 1; fi

    prompt_msg '>>Dealing css,js files.\n' G
    cd ${MY_consoleDir}/env.data
    cp -a ${MY_consoleDir}/function/nodejs/* ./
    gulp build
    if [ $? -ne 0 ]; then return 1; fi
    node revreplace.js
    if [ $? -ne 0 ]; then return 1; fi

    #彻底移除不应该发出外网的危险文件
    prompt_msg '>>Starting remove none production files.\n' G
    local appDir=${DATA_productionDir}/project/app
    rm -rf \
        ${appDir}/controllers/Admin \
        ${appDir}/controllers/Stat \
        ${appDir}/config/dev \
        ${appDir}/config/staging  \
        ${appDir}/database/Database/dev \
        ${appDir}/database/Database/staging
    if [ $? -ne 0 ]; then return 1; fi

    cd ${SCRIPT_DIR}
}

function buildSource() {
    if [ ! -d ${DATA_productionDir}/source ]; then
       mkdir ${DATA_productionDir}/source
    fi

    prompt_msg ">>merging xml ...\n" G
    ${STAG_toolDir}/script/config_center/merge_xml.sh ${STAG_storageDir}/ocm/ > ${DATA_productionDir}/source/merged_config.xml;
    if [ $? -ne 0 ]; then return 1; fi

    prompt_msg ">>rsync source files ...\n" G
    rsync -av --delete ${STAG_projectDir}/public/source/image ${STAG_projectDir}/public/source/wysiwyg ${DATA_productionDir}/source/;
    if [ $? -ne 0 ]; then return 1; fi
}

# ============================== MIAN ==============================

if [ $# -lt 2 ]; then usage; else notice; fi

USERNAME=$(getSvrUser)

SVR_SET=$1
shift

while [ $# -gt 0 ]; do
    case "$1" in
    web)
        PUB_WEB=1
        buildWeb
        exit_status ">>build web."
        ;;
    source)
        PUB_SRC=1
        buildSource
        exit_status ">>build source."
        ;;
    *)
        usage
        ;;
    esac
    shift
done

openPre ${SVR_SET}
for row in $(getSvrList ${SVR_SET}); do
    eval $(echo $row | awk -F':' '{print "IPADDR="$2,"SERVER="$1}')

    if [ $PUB_WEB ]; then
                /usr/bin/rsync -av -e"ssh -p36000 -i ${PROD_IdentityFile}" \
                    ${DATA_productionDir}/project/* ${USERNAME}@${IPADDR}:${PROD_projectPreDir}
                exit_status ">>pre publish web to server_${SERVER} ${IPADDR}"
    fi
    if [ $PUB_SRC ]; then
                /usr/bin/rsync -av -e"ssh -p36000 -i ${PROD_IdentityFile}" \
                    ${DATA_productionDir}/source/* ${USERNAME}@${IPADDR}:${PROD_projectPreDir}/public/source
                exit_status ">>pre publish source(file) to server_${SERVER} ${IPADDR}"
                pssh -H ${USERNAME}@${IPADDR}:36000 -O IdentityFile=${PROD_IdentityFile} -i "sudo su -c \"${PROD_toolPreDir}/publish/prod_source_memory.sh preview all\""
                exit_status ">>pre update source in server_${SERVER} ${IPADDR}"
    fi
done