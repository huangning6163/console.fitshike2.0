#!/bin/bash
SCRIPT_DIR=$(cd "$(dirname "$0")"; pwd)
source ${SCRIPT_DIR}/env.include.sh
cd ${SCRIPT_DIR}

function notice() {
    echo -ne "\nFBI WARNING:\n1，请确认你真的要发 $1 $2 【正式环境】？\n(Y/N)?"
    read yesno
    if [ "$yesno" != "Y" ] && [ "$yesno" != "y" ];
    then
        echo "ABORTED"
        exit
    fi
}

function usage() {
        echo "usage: ./`basename $0` <serverId|global:0> <web|source|web_source>"
        exit 1
}


if [ $# -lt 2 ]; then usage; else notice $1 $2; fi

USERNAME=$(getSvrUser)

SVR_SET=$1
shift

COMMAND=$1
if [ ${COMMAND} == 'web' ] || [ ${COMMAND} == 'source' ] || [ ${COMMAND} = 'web_source' ]; then
    for row in $(getSvrList ${SVR_SET}); do
        eval $(echo $row | awk -F':' '{print "IPADDR="$2,"SERVER="$1}')
        pssh -H ${USERNAME}@${IPADDR}:36000 -O IdentityFile=${PROD_IdentityFile} -i "${PROD_toolPreDir}/publish/prod_multi_release.sh ${COMMAND}"
        exit_status "publish ${COMMAND} to ${SERVER}"
    done
else
    usage
fi

closePre ${SVR_SET}