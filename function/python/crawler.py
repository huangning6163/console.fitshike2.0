#coding=utf-8
#!/usr/bin/python

from consoleGlobal import ConsoleGlobal
import Queue
import urllib
import os

def reporthook(blocknum, blocksize, totalsize):
    '''回调函数
    @blocknum: 已经下载的数据块
    @blocksize: 数据块的大小
    @totalsize: 远程文件的大小
    '''
    percent = 100.0 * blocknum * blocksize / totalsize
    if percent > 100:
        percent = 100
    print "%.2f%%"% percent

def store(url):
    urllib.urlretrieve(url, ConsoleGlobal.crawlerPath+'/163/'+urllib.quote(url).replace('/', '_')+'.html', reporthook)



initPage = "http://www.163.com"

url_queue = Queue.Queue()
seen = set()

seen.add(initPage)
url_queue.put(initPage)
crawlerCount = 0

while (crawlerCount<10):
    if url_queue.qsize() > 0:
        current_url = url_queue.get()
        store(current_url)
        for next_url in extract_urls(current_url):
            if next_url not in seen:
                url_queue.put(next_url)
                seen.add(next_url)
    else:
        break