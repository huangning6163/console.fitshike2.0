#coding=utf-8
#!/usr/bin/python

from consoleGlobal import ConsoleGlobal
import hashlib
import os
import subprocess
import sys

def hashfile(afile, hasher, blocksize=65536):
    buf = afile.read(blocksize)
    while len(buf) > 0:
        hasher.update(buf)
        buf = afile.read(blocksize)
    return hasher.hexdigest()[:16]

def absoluteFilePaths(directory):
   for dirpath,_,filenames in os.walk(directory):
       for f in filenames:
           yield os.path.abspath(os.path.join(dirpath, f))

def replace(replaceListFile, replacePath):
    global hashedList
    replaceCmd = "cat "+replaceListFile+" | xargs sed"
    for fname,checksum in hashedList:
        fname = fname.replace(ConsoleGlobal.prodProjectPath+"/public/assets/img/", replacePath)
        replaceName = fname + '?v=' + checksum
        replaceCmd = replaceCmd + " -i -e 's/" +fname.replace('/','\/')+ "/" +replaceName.replace('/','\/')+ "/g'"
    subprocess.check_call(replaceCmd, shell=True)


imgDir = ConsoleGlobal.prodProjectPath + '/public/assets/img/'
hashedList = [(fname, hashfile(open(fname, 'rb'), hashlib.sha256())) for fname in absoluteFilePaths(imgDir)]

cssListFile = ConsoleGlobal.prodProjectPath+ "/python_css_list.dat"
subprocess.check_call("find " +ConsoleGlobal.prodProjectPath+ "/public/assets/css/ -name *.css > "+cssListFile, shell=True)
replace(cssListFile, "../img/")

viewsListFile = ConsoleGlobal.prodProjectPath+ "/python_views_list.dat"
subprocess.check_call("find " +ConsoleGlobal.prodProjectPath+ "/app/views/ -name *.blade.php > "+viewsListFile, shell=True)
replace(viewsListFile, "/assets/img/")