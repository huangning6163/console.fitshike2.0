#coding=utf-8
#!/usr/bin/python

import Queue
import cookielib
import urllib
import urllib2
import os
from pyquery import PyQuery as pq
#import lxml.etree as etree
import lxml.html.soupparser as soupparser

import sys
sys.path.append('../../')
from consoleGlobal import ConsoleGlobal


def reporthook(blocknum, blocksize, totalsize):
    '''回调函数
    @blocknum: 已经下载的数据块
    @blocksize: 数据块的大小
    @totalsize: 远程文件的大小
    '''
    percent = 100.0 * blocknum * blocksize / totalsize
    if percent > 100:
        percent = 100
    print "%.2f%%"% percent

def store(url):
    urllib.urlretrieve(url, ConsoleGlobal.crawlerPath+'/hupu/'+urllib.quote(url).replace('/', '_')+'.html', reporthook)


cookieFile = 'cookie.txt'
cookie = cookielib.MozillaCookieJar(cookieFile)
cookie.load(cookieFile, ignore_discard=True, ignore_expires=True)
opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(cookie))
#opener.addheaders = [('', '')]

"""
##登录
postData = urllib.urlencode({
    'mobile':'13265883816',
    'authcode':'124066'
})
loginUrl = 'http://passport.hupu.com/pc/mobileregister.action'
result = opener.open(loginUrl, postData)
print(result.read())
cookie.save(ignore_discard=True, ignore_expires=True)
"""

##获取论坛帖子的作者们
authorFile = open('authors.dat', 'w')
authorList = []

for i in range(1, 101):
    try:
        topicUrl = 'http://bbs.hupu.com/fit-'+str(i)
        result = opener.open(topicUrl)
        #print(result.read())
        #exit()
        #document = etree.fromstring(result.read().decode('iso-8859-1'))
        document = soupparser.fromstring(result.read())

        for authorDom in document.xpath('//*[@id="pl"]/tbody[1]/tr'):
            authorList.append(pq(authorDom).find('.p_author a').attr('href')+"\r\n")
    except:
        continue

authorFile.writelines(set(authorList))
authorFile.close()

exit()
initPage = "http://my.hupu.com/18792683/topic"

url_queue = Queue.Queue()
seen = set()

seen.add(initPage)
url_queue.put(initPage)
crawlerCount = 0

while (crawlerCount<10):
    if url_queue.qsize() > 0:
        current_url = url_queue.get()
        store(current_url)
        for next_url in extract_urls(current_url):
            if next_url not in seen:
                url_queue.put(next_url)
                seen.add(next_url)
    else:
        break