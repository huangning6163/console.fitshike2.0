function getSvrList() {
    local svr_set=$1
    local dir=${MY_consoleDir}/env.data

    if [ $svr_set == '0' ]; then
        cat ${dir}/ips.dat
    else
        grep $svr_set ${dir}/ips.dat
    fi
}

function getSvrUser() {
    echo angelhere
}

function openPre {
    local svr_set ipaddr
    username=$(getSvrUser)
    svr_set=$1

    for row in $(getSvrList ${svr_set}); do
        eval $(echo $row | awk -F':' '{print "ipaddr="$2,"server="$1}')

        pssh -H ${username}@${ipaddr}:36000 -O IdentityFile=${PROD_IdentityFile} -i "if [ ! -d \"${PROD_projectPreDir}\" ]; then mv ${PROD_projectPreDir}_closed ${PROD_projectPreDir}; fi"
        exit_status "open ${server} preview"
    done
}

function closePre {
    local svr_set ipaddr
    svr_set=$1
    username=$(getSvrUser)

    for row in $(getSvrList ${platform} ${svr_set}); do
        eval $(echo $row | awk -F':' '{print "ipaddr="$2,"server="$1}')

        pssh -H ${username}@${ipaddr}:36000 -O IdentityFile=${PROD_IdentityFile} -i "if [ ! -d \"${PROD_projectPreDir}_closed\" ]; then mv ${PROD_projectPreDir} ${PROD_projectPreDir}_closed; fi"
        exit_status "close ${server} preview"
    done
}