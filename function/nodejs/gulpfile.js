var global = require('./global');
var fs = require('fs');
var gulp = require('gulp');
var glob = require('glob');
var uglify = require('gulp-uglify');
var rev = require('gulp-rev');
var del = require('del');
//var filename = require('gulp-asset-manifest');
var minifycss = require('gulp-minify-css');
var debug = require('gulp-debug');
//var template = require('gulp-template');
var filelist = require('gulp-filelist');
var util = require('util');
//var imagemin = require('gulp-imagemin');

var projectPath = global.prodProjectPath();

// CSS task
function css() {
    // Cleanup old assets
    //del(['public/assets/css/styles-*.css'], function (err) {});

    // Prefix, compress and concat the CSS assets
    // Afterwards add the MD5 hash to the filename
    !fs.existsSync(projectPath+'rev_css') && fs.mkdirSync(projectPath+'rev_css', 0774);

    gulp.src(projectPath+'public/assets/css/**/*.css')
        .pipe(rev())
        .pipe(minifycss())
        .pipe(gulp.dest(projectPath+'public/assets/css'))
        .pipe(rev.manifest())
        .pipe(gulp.dest(projectPath+'rev_css/'));
};

// JavaScript task
function js() {
    // Cleanup old assets
    //del(['public/assets/js/scripts-*.js'], function (err) {});

    // Concat and uglify the JavaScript assets
    // Afterwards add the MD5 hash to the filename
    !fs.existsSync(projectPath+'rev_js') && fs.mkdirSync(projectPath+'rev_js', 0774);

    gulp.src(projectPath+'public/assets/js/**/*.js')
        .pipe(uglify())
        .pipe(rev())
        .pipe(gulp.dest(projectPath+'public/assets/js'))
        .pipe(rev.manifest())
        .pipe(gulp.dest(projectPath+'rev_js/'));
};


gulp.task('css', css);
gulp.task('js', ['css'], js);
gulp.task('build', ['js']);