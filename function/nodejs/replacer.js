var fs = require('fs');
//var exec = require('child_process').exec;
var sh = require('execSync');

function parseMap(fileFullName) {
    return JSON.parse(fs.readFileSync(fileFullName));
};

function replace(mapFile, mapBasePath, replaceFilesGlob) {
    var map = parseMap(mapFile);
    execCmd = replaceFilesGlob+ ' | xargs sed';
    for(var mapKey in map) {
        var needleStr = mapBasePath + mapKey;
        var replaceStr = mapBasePath + map[mapKey];

        execCmd += ' -i -e "s/' +needleStr.replace(new RegExp('/', 'g'), '\\/')+ '/' +replaceStr.replace(new RegExp('/', 'g'), '\\/')+ '/g"';

        /*for(var i=0; i<replaceFiles.length; i++) {
         fs.readFileSync(replaceFiles[i], 'utf8', function (err,data) {
         if (err) {
         return console.log(err);
         }
         var result = data.replace(/string to be replaced/g, 'replacement');

         fs.writeFileSync(replaceFiles[i], result, 'utf8', function (err) {
         if (err) return console.log(err);
         });
         });
         }*/
    }

    //console.log(execCmd);
    var result = sh.exec(execCmd);
    console.log('>>replace return code: ' + result.code);
    console.log('>>replace stdout + stderr: ' + result.stdout);
    /*exec(execCmd, {encoding: 'utf8'}, function(error, stdout, stderr){
     if (error !== null) {
     console.log('>>find views exec error: ' + error);
     return;
     }
     });*/
};

exports.replace = replace;