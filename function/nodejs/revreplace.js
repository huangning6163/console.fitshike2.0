var replacer = require('./replacer');
var global = require('./global');

var projectPath = global.prodProjectPath();

console.log('>>starting replace css revision.');
replacer.replace(projectPath+'rev_css/rev-manifest.json', '/assets/css/', "find "+projectPath+"app/views/ -name '*.php'");

console.log('>>starting replace js revision.');
replacer.replace(projectPath+'rev_js/rev-manifest.json', '/assets/js/', "find "+projectPath+"app/views/ -name '*.php'");