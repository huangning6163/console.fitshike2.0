#!/bin/bash
SCRIPT_DIR=$(cd "$(dirname "$0")"; pwd)
source ${SCRIPT_DIR}/env.include.sh

cd ${DEV_projectDir}
if [ ! -f "${DEV_projectDir}/composer.json" ]; then
  prompt_msg "Dec_projectDir defined wrong!!! \n" R 
  exit
fi

php artisan optimize --force
cd ${SCRIPT_DIR}

# when using --delete option,  maybe we should add --exclude="/app/storage/*" etc., to avoid deleting of remote storage/files. 
rsync -av -e"ssh -p36000" --delete \
	--exclude=".git*" \
	--exclude=".svn*" \
	--exclude="/app/storage" \
	--exclude="/public/source" \
	--exclude="/public/upload" \
	--exclude="/tool/env.*" \
    ${DEV_projectDir}/ www@218.244.135.29:${STAG_projectDir}/


exit_status "publish src to staging server "

