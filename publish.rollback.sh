#!/bin/bash
SCRIPT_DIR=$(cd "$(dirname "$0")"; pwd)
source ${SCRIPT_DIR}/env.include.sh
cd ${SCRIPT_DIR}

function notice() {
    echo -ne "\nFBI WARNING:\n1，哇塞!真的要回滚 $1 $2 【正式环境】？\n(Y/N)?"
    read yesno
    if [ "$yesno" != "Y" ] && [ "$yesno" != "y" ];
    then
        echo "ABORTED"
        exit
    fi
}

function usage() {
        echo "usage: ./`basename $0` <serverId|global:0> <web|source|web_source>"
        exit 1
}


if [ $# -lt 2 ]; then usage; else notice $1 $2; fi

USERNAME=$(getSvrUser)

SVR_SET=$1
shift

## 一旦发正式就必然关闭pre
closePre

COMMAND=$1
if [ ${COMMAND} == 'web' ] || [ ${COMMAND} == 'source' ] || [ ${COMMAND} = 'web_source' ]; then
    if [ ${COMMAND} = 'web_source' ]; then COMMAND='web source'; fi
    for row in $(getSvrList ${SVR_SET}); do
        eval $(echo $row | awk -F':' '{print "IPADDR="$2,"SERVER="$1}')
        pssh -H ${USERNAME}@${IPADDR}:36000 -O IdentityFile=${PROD_IdentityFile} -i "${PROD_toolPreDir}/publish/prod_multi_rollback.sh ${COMMAND}"
        exit_status "publish ${COMMAND} to ${SERVER}"
    done
else
    usage
fi
